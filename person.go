package populationaudit

import(
	"encoding/json"
	"fmt"
)

type Person struct{
	Id   string  `json:"id" gorm:"primary_key"` //身份证号 
	Name string  `json:"name" gorm:"not null"`//名字
	AliasName string `json:"aliasName" gorm:""` //别名
	Birth string `json:"birth" gorm:"not null"` //生日
	Sex byte `json:"sex" gorm:"not null"`  //性别
	Career byte `json:"career" gorm:"not null"` //职业
	Addres string `json:"addres" gorm:"not null"`  //住址
	Enterprise string `json:"enterprise" gorm:"not null"`  //公司
	Village string `json:"village" gorm:"not null"`  //小区
	Father string `json:"father" gorm:"not null"`  //父亲
	Mather string `json:"mather" gorm:"not null"`  //母亲
	Mate string `json:"mate" gorm:""`  //配偶
}

func (man Person)toString()(string)  {
	data, err := json.Marshal(man)
	if err != nil {
		return fmt.Sprintf("json marshal error: %s", err.Error())
	} else {
		return string(data)
	}
}